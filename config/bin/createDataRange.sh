#!/bin/bash
for  i in $(seq -429 3600) 
do
	if [[ ${i} -eq 0 ]]; then
		date  -u +\"%Y-%m-%d\"  | tr -d '"'
	elif [[ ${i} -gt 0 ]]; then
		date  -v+${i}d  -u +\"%Y-%m-%d\"  | tr -d '"'
	else
		date  -v${i}d  -u +\"%Y-%m-%d\"  | tr -d '"'
	fi
done

