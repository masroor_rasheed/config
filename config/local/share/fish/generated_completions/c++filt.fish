# c++filt
# Autogenerated from man page /Library/Developer/CommandLineTools/usr/share/man/man1/c++filt.1
complete -c c++filt -l format -s s -d 'Mangling scheme to assume'
complete -c c++filt -l help -s h -d 'Print a summary of command line options'
complete -c c++filt -l help-list -d 'Print an uncategorized summary of command line options'
complete -c c++filt -l strip-underscore -s _ -d 'Discard a single leading underscore, if present, from each input name before …'
complete -c c++filt -l types -s t -d 'Attempt to demangle names as type names as well as function names'
complete -c c++filt -l version -d 'Display the version of the llvm-cxxfilt executable'

