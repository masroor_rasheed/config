# dbiprof5
# Autogenerated from man page /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/share/man/man1/dbiprof5.18.1
complete -c dbiprof5 -l number -d 'Produce this many items in the report.   Defaults to 10'
complete -c dbiprof5 -l sort -d 'Sort results by the given field'
complete -c dbiprof5 -l reverse -d 'Reverses the selected sort'
complete -c dbiprof5 -l match -d 'Consider only items where the specified key matches the given value'
complete -c dbiprof5 -l exclude -d 'Remove items for where the specified key matches the given value'
complete -c dbiprof5 -l case-sensitive -d 'Using this option causes --match and --exclude to work case-sensitively'
complete -c dbiprof5 -l delete -d 'Sets the \\f(CW\\*(C`DeleteFiles\\*(C\' option to DBI::ProfileData which causes t…'
complete -c dbiprof5 -l dumpnodes -d 'Print the list of nodes in the form of a perl data structure'

