# exfat
# Autogenerated from man page /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/share/man/man8/exfat.util.8
complete -c exfat -s p -d 'Probe the device to determine whether it contains an ExFAT file system'
complete -c exfat -s k -d 'Return the volume UUID of the ExFAT file system on device'
complete -c exfat -s s -d 'Set a new UUID for the ExFAT file system on device, which must not be mounted'
complete -c exfat -s m -d 'Deprecated.  Mount the ExFAT file system from device onto directory'
complete -c exfat -s u -d 'Deprecated.  Unmount the file system on device'

