# ln
# Autogenerated from man page /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/share/man/man1/ln.1
complete -c ln -s F -d 'If the proposed link (link_name) already exists and is a directory, then remo…'
complete -c ln -s h -d 'If the link_name or link_dirname is a symbolic link, do not follow it'
complete -c ln -s f -d 'If the proposed link (link_name) already exists, then unlink it so that the l…'
complete -c ln -s i -d 'Cause ln ln to write a prompt to standard error if the proposed link exists'
complete -c ln -s n -d 'Same as h, for compatibility with other ln ln implementations'
complete -c ln -s s -d 'Create a symbolic link'
complete -c ln -s v -d 'Cause ln ln to be verbose, showing files as they are processed'

