# python
# Autogenerated from man page /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/share/man/man1/python.1
complete -c python -s B -d 'Don\'t write . py[co] files on import.  See also PYTHONDONTWRITEBYTECODE'
complete -c python -s c -d 'Specify the command to execute (see next section)'
complete -c python -s d -d 'Turn on parser debugging output (for wizards only, depending on compilation o…'
complete -c python -s E -d 'Ignore environment variables like PYTHONPATH and PYTHONHOME that modify the b…'
complete -c python -s h -s '?' -l help -d 'Prints the usage for the interpreter executable and exits'
complete -c python -s i -d 'When a script is passed as first argument or the -c option is used, enter int…'
complete -c python -s m -d 'Searches  sys. path  for the named module and runs the corresponding  '
complete -c python -s O -d 'Turn on basic optimizations'
complete -c python -o OO -d 'Discard docstrings in addition to the -O optimizations'
complete -c python -s R -d 'Turn on "hash randomization", so that the hash() values of str, bytes and dat…'
complete -c python -s Q -d 'Division control; see PEP 238'
complete -c python -s s -d 'Don\'t add user site directory to sys. path'
complete -c python -s S -d 'Disable the import of the module site and the site-dependent manipulations of…'
complete -c python -s t -d 'Issue a warning when a source file mixes tabs and spaces for indentation in a…'
complete -c python -s u -d 'Force stdin, stdout and stderr to be totally unbuffered'
complete -c python -s v -d 'Print a message each time a module is initialized, showing the place (filenam…'
complete -c python -s V -l version -d 'Prints the Python version number of the executable and exits'
complete -c python -s W -d 'Warning control.   Python sometimes prints warning message to sys. stderr '
complete -c python -s x -d 'Skip the first line of the source'
complete -c python -s 3 -d 'Warn about Python 3. x incompatibilities that 2to3 cannot trivially fix'

