# rm
# Autogenerated from man page /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/share/man/man1/rm.1
complete -c rm -s d -d 'Attempt to remove directories as well as other types of files'
complete -c rm -s f -d 'Attempt to remove the files without prompting for confirmation, regardless of…'
complete -c rm -s i -d 'Request confirmation before attempting to remove each file, regardless of the…'
complete -c rm -s P -d 'Overwrite regular files before deleting them'
complete -c rm -s R -d 'Attempt to remove the file hierarchy rooted in each file argument'
complete -c rm -s r -d 'Equivalent to R'
complete -c rm -s v -d 'Be verbose when deleting files, showing them as they are removed'
complete -c rm -s W -d 'Attempt to undelete the named files'

