# rpc
# Autogenerated from man page /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/share/man/man8/rpc.statd.8
complete -c rpc -s n -d 'Send SM_NOTIFY messages to notify any hosts of a restart'
complete -c rpc -s l -d 'List each host (and its status) in the status file'
complete -c rpc -s L -d 'List each host (and its status) in the status file and then continue to watch…'
complete -c rpc -s N -d 'Clear the "needs notification" status for hostname so the statd'

