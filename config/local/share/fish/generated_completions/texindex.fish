# texindex
# Autogenerated from man page /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/share/man/man1/texindex.1
complete -c texindex -s h -l help -d 'display this help and exit'
complete -c texindex -s k -l keep -d 'keep temporary files around after processing'
complete -c texindex -l no-keep -d 'do not keep temporary files around after processing (default)'
complete -c texindex -s o -l output -d 'send output to FILE'
complete -c texindex -l version -d 'display version information and exit REPORTING BUGS Email bug reports to bug-…'

