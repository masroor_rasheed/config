set nocompatible		" Make vim more useful

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Looks
Plug 'morhetz/gruvbox'

"Git"
Plug 'airblade/vim-gitgutter'

" Initialize plugin system
call plug#end()

filetype plugin indent on
set omnifunc=syntaxcomplete#Complete
syntax on
set hidden
colorscheme gruvbox
set background=dark
set fileformat=unix
set fileencoding=utf-8
set encoding=utf-8 nobomb
set laststatus=2
set clipboard=unnamed
set mouse=nicra
let mapleader=" "
set path+=**
set wildmenu			" Enhance command-line completion
set wildmode=list:full
set ttyfast			" Optamize for fast terminal
set gdefault			" Add the g flag to search/replace by default
set nu rnu
set cursorline
set ruler
set scrolloff=5
set showmode
set showcmd
set title
set hlsearch			" Highlight searches
set ignorecase			" Ignore cases in searches
set incsearch			" Highlight dynamically
setlocal spell spelllang=en_gb	" British Dictionary
set spell 				" Enable spelling check
"set dictionary?
set dictionary+=/usr/share/dict/words
set complete+=k			" Enable dictionary complete
set completeopt=longest,menuone
set nohlsearch
set smartindent
set showmatch
set nobackup
set nowritebackup
set cmdheight=2
set updatetime=300
set shortmess+=c
set signcolumn=yes

" Text, Tab and indent related

set smarttab
set shiftwidth=4
set tabstop=4

" Enable folding
set foldmethod=indent
set foldlevel=99

" Status line
set statusline=			" Left side of the bar
set statusline+=%#DiffChange#
set statusline+=\ %M	" Any changes to file made
set statusline+=\ %y
set statusline+=\ %r
set statusline+=%#DiffAdd#
set statusline+=\ %F	" Full file name with path
set statusline+=%=		" Right side of the bar
set statusline+=%#TermCursor#
set statusline+=\ %c:(%l/%L)
set statusline+=\ %p%%
set statusline+=%#Conceal#
set statusline+=\ [%n]

set splitbelow splitright

" Changing Enter Behviour for popup"
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
  \ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
inoremap <expr> <M-,> pumvisible() ? '<C-n>' :
  \ '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
" open omni completion menu closing previous if open and opening new menu without changing the text
inoremap <expr> <C-Space> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
            \ '<C-x><C-o><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>" : ""<CR>'
" open user completion menu closing previous if open and opening new menu without changing the text
inoremap <expr> <S-Space> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
            \ '<C-x><C-u><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>" : ""<CR>'


"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" Make adjusting split sizes a bit more friendly
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>
" Change 2 split windows from vertical to horizontal and vice versa
map <Leader>th	<C-w>t<C-w>H
map <Leader>tk	<C-w>t<C-w>K

" Removes pipes | that act as separators on splits
set fillchars+=vert:\


" Show invisible characters
" set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
" set list

" Mode Settings for Cursor
" 1 -> blinking block
" 2 -> solid block
" 3 -> blinking underscore
" 4 -> solid underscore
" 5 -> blinking vertical bar
" 6 -> solid vertical block
let &t_SI.="\e[5 q" 	"SI = INSERT mode
let &t_SR.="\e[3 q"	"SR = REPLACE mode
let &t_EI.="\e[1 q"	"EI = NORMAL mode


" Auto close brackets and quotation
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

let s:comment_map = { 
    \   "c": '\/\/',
    \   "cpp": '\/\/',
    \   "go": '\/\/',
    \   "java": '\/\/',
    \   "javascript": '\/\/',
    \   "lua": '--',
    \   "scala": '\/\/',
    \   "php": '\/\/',
    \   "python": '#',
    \   "ruby": '#',
    \   "rust": '\/\/',
    \   "sh": '#',
    \   "desktop": '#',
    \   "fstab": '#',
    \   "conf": '#',
    \   "profile": '#',
    \   "bashrc": '#',
    \   "bash_profile": '#',
    \   "mail": '>',
    \   "eml": '>',
    \   "bat": 'REM',
    \   "ahk": ';',
    \   "vim": '"',
    \   "tex": '%',
    \   "sql": '--',
    \   "yaml": '#',
    \ }

function! ToggleComment()
    if has_key(s:comment_map, &filetype)
        let comment_leader = s:comment_map[&filetype]
        if getline('.') =~ "^\\s*" . comment_leader . " " 
            " Uncomment the line
            execute "silent s/^\\(\\s*\\)" . comment_leader . " /\\1/"
        else 
            if getline('.') =~ "^\\s*" . comment_leader
                " Uncomment the line
                execute "silent s/^\\(\\s*\\)" . comment_leader . "/\\1/"
            else
                " Comment the line
                execute "silent s/^\\(\\s*\\)/\\1" . comment_leader . " /"
            end
        end
    else
        echo "No comment leader found for filetype"
    end
endfunction


nnoremap <leader>c :call ToggleComment()<cr>
vnoremap <leader>u :call ToggleComment()<cr>

" for hex editing
augroup Binary
    au!
    au BufReadPre  *.bin let &bin=1
    au BufReadPost *.bin if &bin | %!xxd
    au BufReadPost *.bin set ft=xxd | endif
    au BufWritePre *.bin if &bin | %!xxd -r
    au BufWritePre *.bin endif
    au BufWritePost *.bin if &bin | %!xxd
    au BufWritePost *.bin set nomod | endif
augroup END

" Python Files
au BufNewFile,BufRead *.py
            \ set expandtab       |" replace tabs with spaces
            \ set autoindent      |" copy indent when starting a new line
            \ set tabstop=4
            \ set softtabstop=4
            \ set shiftwidth=4
            \ set foldmethod=indent
" "au BufWrite *.py :Autoformat

" JSON Files"
au BufWrite *.json :%python -m json.tool

" YAML Files"
autocmd FileType yml setlocal ts=2 sts=2 sw=2 expandtab 
let g:indentLine_char = '⦙'



" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction


