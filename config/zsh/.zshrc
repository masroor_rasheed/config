# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config//zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

################################################################################
# File Name : .zshrc 
# Author : Masroor Rasheed
# Purpose : Should be used for the shell configuration and command execution 
################################################################################

autoload -Uz compinit colors vcs_info promptinit
compinit
colors
promptinit

zmodload zsh/nearcolor
zstyle :prompt:pure:path color '#FF0000'

fpath=($fpath "/Users/masroor/.zfunctions")
# prompt spaceship

precmd_vsc_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst

REPORTTIME=3        # Report command running time if it more than 3 seconds
setopt HIST_SAVE_NO_DUPS         # Do not write a duplicate event to the history file.
setopt AUTO_PUSHD           # Push the current directory visited on the stack.
setopt PUSHD_IGNORE_DUPS    # Do not store duplicates in the stack.
setopt PUSHD_SILENT         # Do not print the directory stack after pushd or popd.
# Add commands to history as they are entered, don't wait for shell to exit
setopt INC_APPEND_HISTORY
setopt EXTENDED_HISTORY     # Also remember command start time and duration
setopt HIST_IGNORE_ALL_DUPS # Do not keep duplicate commands in history
setopt HIST_IGNORE_SPACE    # Do not remember commands that start with a white space
setopt CORRECT_ALL          # Correct spelling of all arguments in the command line
# Enable autocompletion
zstyle ':completion:*' completer _complete _correct _approximate 
zstyle ':completion:ls:*' menu yes select
zstyle ':completion:*:default' list-colors \
    "di=34:ln=35:so=32:pi=33:ex=31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43"

# optionally define some options
PURE_CMD_MAX_EXEC_TIME=10

# change the path color
zstyle :prompt:pure:path color white

# Format the vcs_info_msg_0_ variable
zstyle ':vcs_info:git:*' formats 'on branch %b'

# change the color for both `prompt:success` and `prompt:error`
zstyle ':prompt:pure:prompt:*' color cyan

# turn on git stash status
zstyle :prompt:pure:git:stash show yes

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' get-revision true
zstyle ':vcs_info:*' stagedstr '%F{green}✚%f'
zstyle ':vcs_info:*' unstagedstr '%F{yellow}●%f'
zstyle ':vcs_info:*' formats ' %u%c'
zstyle ':vcs_info:*' actionformats ' %u%c'
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git*' formats "%F{blue}%b%f %u%c"
zstyle ':vcs_info:git:*' formats ' herp'

export TERM="xterm-256color" CLICOLOR=1
export LESS_TERMCAP_mb=$(print -P "%F{cyan}") \
    LESS_TERMCAP_md=$(print -P "%B%F{red}") \
    LESS_TERMCAP_me=$(print -P "%f%b") \
    LESS_TERMCAP_so=$(print -P "%K{magenta}") \
    LESS_TERMCAP_se=$(print -P "%K{black}") \
    LESS_TERMCAP_us=$(print -P "%U%F{green}") \
    LESS_TERMCAP_ue=$(print -P "%f%u")


PROMPT='%(?.%F{green}√.%F{red}?%?)%f %B%F{240}%1~%f%b %# '
RPROMPT=\$vcs_info_msg_0_


# _setup_ps1() {
  # vcs_info
  # GLYPH="▲"
  # [ "x$KEYMAP" = "xvicmd" ] && GLYPH="▼"
  # PS1=" %(?.%F{blue}.%F{red})$GLYPH%f %(1j.%F{cyan}[%j]%f .)%F{blue}%~%f %(!.%F{red}#%f .)"
  # RPROMPT="$vcs_info_msg_0_"
# }
# _setup_ps1
# 
# # Vi mode
# zle-keymap-select () {
 # _setup_ps1
  # zle reset-prompt
# }
# zle -N zle-keymap-select
# zle-line-init () {
  # zle -K viins
# }
# zle -N zle-line-init


# VIM Mode
bindkey -v
export KEYTIMEOUT=1
zmodload zsh/complist
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
# Allow to edit the command in editor
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

################################################################################
# Type : VIM Mode at Shell  
# Purpose : Ensure the mode is understood by cursor shape
################################################################################
cursor_mode() {
    # See https://ttssh2.osdn.jp/manual/4/en/usage/tips/vim.html for cursor shapes
    cursor_block='\e[2 q'
    cursor_beam='\e[6 q'

    function zle-keymap-select {
        if [[ ${KEYMAP} == vicmd ]] ||
            [[ $1 = 'block' ]]; then
            echo -ne $cursor_block
        elif [[ ${KEYMAP} == main ]] ||
            [[ ${KEYMAP} == viins ]] ||
            [[ ${KEYMAP} = '' ]] ||
            [[ $1 = 'beam' ]]; then
            echo -ne $cursor_beam
        fi
    }

    zle-line-init() {
        echo -ne $cursor_beam
    }

    zle -N zle-keymap-select
    zle -N zle-line-init
}

cursor_mode

compaudit | xargs chmod g-w

export VISUAL=nvim
export EDITOR=nvim

export HISTFILE="$ZDOTDIR/.zhistory"    # History filepath
export HISTSIZE=100000                   # Maximum events for internal history
export SAVEHIST=1000000                   # Maximum events in history file


source ${XDG_CONFIG_HOME}/bin/aliases

export PATH="/opt/homebrew/opt/python@3.9/libexec/bin:$PATH"
export PATH="/opt/homebrew/opt/luajit-openresty/bin:$PATH"

export PATH="/opt/homebrew/opt/ncurses/bin:$PATH"
# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH


# To customize prompt, run `p10k configure` or edit ~/.config//zsh/.p10k.zsh.
[[ ! -f ~/.config//zsh/.p10k.zsh ]] || source ~/.config//zsh/.p10k.zsh
export PATH="/opt/homebrew/opt/openjdk/bin:$PATH"
export PATH="/opt/homebrew/opt/ruby/bin:$PATH"
export LDFLAGS="-L/opt/homebrew/opt/ruby/lib"
export CPPFLAGS="-I/opt/homebrew/opt/ruby/include"
export PKG_CONFIG_PATH="/opt/homebrew/opt/ruby/lib/pkgconfig"
