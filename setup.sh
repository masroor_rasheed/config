#!/bin/bash

ARCH=$(uname -p)
SHELL_PATH=$(cd $(dirname $0) > /dev/null 2>&1 ; pwd -P)
echo "Architecture : " ${ARCH}
echo "Script Directory : " ${SHELL_PATH} 

GBL_ZSHENV="/etc/zshenv"
XDG_CONFIG_HOME="$HOME/.config"
GBL_VIM="$HOME/.vim"

link2repo(){
	if [ ! -e $1 ]; then
		echo "Link Repo to : " $2
		ln -s $2 $1
	fi
}


if [ ! -e ${GBL_ZSHENV} ]; then
	sudo ln -s  ${SHELL_PATH}/etc/zsh/zshenv ${GBL_ZSHENV} 
	echo "Link created for : " ${GBL_ZSHENV}
fi

link2repo $XDG_CONFIG_HOME ${SHELL_PATH}/config
link2repo ${GBL_VIM} ${SHELL_PATH}/config/vim
# 
